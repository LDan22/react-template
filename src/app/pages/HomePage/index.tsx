import * as React from 'react'
import { Helmet } from 'react-helmet-async'
import { P } from './P'

export function HomePage() {
  return (
    <>
      <Helmet>
        <title>Home Page</title>
        <meta name="description" content="A Boilerplate application homepage" />
      </Helmet>
      <P>HomePage container</P>
    </>
  )
}
