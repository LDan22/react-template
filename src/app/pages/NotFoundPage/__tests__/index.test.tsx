import * as React from 'react'
import { NotFoundPage } from '..'
import { MemoryRouter } from 'react-router-dom'
import { HelmetProvider } from 'react-helmet-async'
import renderer from 'react-test-renderer'
import { P } from '../P'

const renderPage = () =>
  renderer.create(
    <MemoryRouter>
      <HelmetProvider>
        <NotFoundPage />
      </HelmetProvider>
    </MemoryRouter>,
  )

describe('<NotFoundPage />', () => {
  it('should match snapshot', () => {
    const notFoundPage = renderPage()
    expect(notFoundPage.toJSON()).toMatchSnapshot()
  })

  it('should should contain Link', () => {
    const notFoundPage = renderPage()
    expect(notFoundPage.root.findByType(P)).toBeDefined()
  })
})
