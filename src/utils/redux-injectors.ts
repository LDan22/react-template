import {
  useInjectReducer as useReducer,
  useInjectSaga as useSaga,
} from 'redux-injectors'
import {
  InjectReducerParams,
  InjectSagaParams,
  RootStateKeyType,
} from './types/injector-typings'

/* Wrap redux-injectors with stricter types */

/* istanbul ignore next */
export function useInjectReducer<Key extends RootStateKeyType>(
  params: InjectReducerParams<Key>,
) {
  return useReducer(params)
}

/* istanbul ignore next */
export function useInjectSaga(params: InjectSagaParams) {
  return useSaga(params)
}
